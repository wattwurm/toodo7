package com.wattwurm.toodoo

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.wattwurm.toodoo.data.*
import com.wattwurm.toodoo.databinding.FragmentDetailBinding
import java.text.SimpleDateFormat

class FragmentDetail : Fragment() {

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.i("DEBUG", "FragmentDetail.onCreateView")

        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.fragment_detail, container, false)
        //val binding = FragmentDetailBinding.inflate(inflater)
        // this call if inflate (with argument container!) is needed to fill the whole screen with the root view of fragment_detail..xml
        val binding = FragmentDetailBinding.inflate(inflater, container, false)

        val act = activity as MainActivity
        // binding.textViewTName.text = act.items[act.currentItemPos]

        val currentTask = if(act.appState.tasks.taskCreateIsPending) {
            val newTask = Task()
            newTask.name = ""
            newTask.category = act.appState.tasks.singleCategoryIfExisting ?: act.appState.categories.itemAtPosition(0)
            newTask.dueDate = TDate.today
            newTask
        } else {
            // act.appState.tasks.taskList[act.appState.tasks.currentItemPos]
            act.appState.tasks.currentTask
        }

        binding.editTextTsk.setText(currentTask.name)
        binding.editTextTsk.setSelection(binding.editTextTsk.text.length) // moves cursor to end of text
        binding.editTextDesc.setText(currentTask.desc)

        // a SpinnerAdapter is needed, and ArrayAdapter implements SpinnerAdapter
        //val spAdapter = ArrayAdapter<Category>(this.context!!, R.layout.support_simple_spinner_dropdown_item, act.appState.categories.iterator)
        val spAdapter = ArrayAdapter<String>(this.requireContext(), R.layout.support_simple_spinner_dropdown_item, act.appState.categories.allCategoryNames)
        binding.catSpinner.adapter = spAdapter
        binding.catSpinner.setSelection(spAdapter.getPosition(currentTask.category.name))

        binding.editDueDate.setText(currentTask.dueDate?.toStorageRep() ?: "")
        binding.editDueTime.setText(currentTask.dueTime?.toStorageRep() ?: "")

        // only works because editDueDate is not focusable
        // otherwise first click would open keyboard, only second click opens datePicker
        binding.editDueDate.setOnClickListener {
            val dateSetListener =
                DatePickerDialog.OnDateSetListener { picker, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in textbox
                    // binding.editDueDate.setText("$year-$monthOfYear-$dayOfMonth")
                    val newTDate = TDate(year.toShort(), (monthOfYear + 1).toByte(), dayOfMonth.toByte())
                    val newTDateString = newTDate.toStorageRep()
                    binding.editDueDate.setText(newTDateString)
//                val newDate = Calendar.getInstance()
//                newDate.set(year, monthOfYear, dayOfMonth)
//                val sdf = SimpleDateFormat("yyyy-MM-dd")
//                val dateString = sdf.format(newDate.time)
//                binding.editDueDate.setText(dateString)
                }
//            val dpd = DatePickerDialog(act)
//            this constructor with one parameter act does not work
//            it throws  java.lang.IllegalAccessError: Method 'void android.app.AlertDialog.<init>(android.content.Context)' is inaccessible to class '....FragmentDetail$onCreateView$1'
//            dpd.setOnDateSetListener(dateSetListener)

            val dateString = binding.editDueDate.text.toString().trim()
            val tDate = TDate.fromStorageRep(dateString) ?: TDate.today
            val dpd = DatePickerDialog(
                act,
                dateSetListener,
                tDate.year.toInt(),
                tDate.month.toInt() - 1,  // one month difference
                tDate.day.toInt()
            )

//            val sdf = SimpleDateFormat("yyyy-MM-dd")
//            val currentDate = Calendar.getInstance() // today
//            Log.i("DEBUG", "editDueDate, today is $currentDate")
//            try {
//                val dateString = binding.editDueDate.text.toString().trim()
//                Log.i("DEBUG", "editDueDate, dateString is $dateString")
//                if (dateString.isNotBlank()) currentDate.time = sdf.parse(dateString)
//            } catch (ex: Exception ) {
//                Log.i("DEBUG", "editDueDate, exception parsing dateString $ex")
//                // ok, if parsing dateString goes wrong keep today
//            }
//            val year = currentDate.get(Calendar.YEAR)
//            val month = currentDate.get(Calendar.MONTH)
//            val day = currentDate.get(Calendar.DAY_OF_MONTH)
//            val dpd = DatePickerDialog(act, dateSetListener, year, month, day)

            dpd.setButton(DialogInterface.BUTTON_NEUTRAL, "Delete Date") { dialog, which ->
                binding.editDueDate.setText("")
                binding.editDueTime.setText("")
            }
            dpd.show()
        }

        binding.editDueTime.setOnClickListener {
            if (binding.editDueDate.text.toString().isNotEmpty()) {
                // duetime only allowed if duedate existing
                val timeSetListener =
                    TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                        // binding.editDueTime.setText("$hour:$minute")
                        val newTTime = TTime(hour.toByte(), minute.toByte())
                        val newTTimeString = newTTime.toStorageRep()
                        binding.editDueTime.setText(newTTimeString)
//                val newDate = Calendar.getInstance()
//                newDate.set(1970, 0, 1, hour, minute)
//                val sdf = SimpleDateFormat("HH:mm")
//                val dateString = sdf.format(newDate.time)
//                binding.editDueTime.setText(dateString)
                    }

//            var hour = 12
//            var minute = 0
//            try {
//                val sdf = SimpleDateFormat("HH:mm")
//                val currentDate = Calendar.getInstance() // today
//                val timeString = binding.editDueTime.text.toString().trim()
//                Log.i("DEBUG", "editDueTime, timeString is $timeString")
//                if (timeString.isNotBlank()) {
//                    currentDate.time = sdf.parse(timeString)
//                    hour = currentDate.get(Calendar.HOUR_OF_DAY)
//                    minute = currentDate.get(Calendar.MINUTE)
//                }
//            } catch (ex: Exception ) {
//                Log.i("DEBUG", "editDueDate, exception parsing timeString $ex")
//                // ok, if parsing timeString goes wrong keep 12:00
//            }

                val timeString = binding.editDueTime.text.toString()
                val tTime = TTime.fromStorageRep(timeString) ?: TTime.nextFullHour
                val tpd = TimePickerDialog(
                    act,
                    timeSetListener,
                    tTime.hour.toInt(),
                    tTime.minute.toInt(),
                    true
                )
                tpd.setButton(DialogInterface.BUTTON_NEUTRAL, "Delete Time") { dialog, which ->
                    binding.editDueTime.setText("")
                }
                tpd.show()
            }
        }

//        alternative solution with setOnTouchListener instead setOnClickListener
//        binding.editDueDate.setOnTouchListener { view, event ->
////            Toast.makeText(
////                this.context,
////                "editDueDate.setOnTouchListener ${event}",
////                Toast.LENGTH_LONG
////            ).show()
//            if (event.action == MotionEvent.ACTION_UP) {
//                // this is necessary for DatePickerDialog to show up only once, after the finger was released from the EditText
//                // otherwise, it would shop up twice
//                val dateSetListener =
//                    DatePickerDialog.OnDateSetListener { picker, year, monthOfYear, dayOfMonth ->
//                        // Display Selected date in textbox
//                        binding.editDueDate.setText("$year-$monthOfYear-$dayOfMonth")
//                    }
//                val dpd = DatePickerDialog(act, dateSetListener, 2020, 10, 27)
//                dpd.show()
//            }
//            true
//        }

//        binding.editDueTime.setOnTouchListener { view, event ->
//            if (event.action == MotionEvent.ACTION_UP) {
//                val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
//                    binding.editDueTime.setText("$hour:$minute")
//                }
//                val tpd = TimePickerDialog(act, timeSetListener, 15, 53, true)
//                tpd.show()
//            }
//            true
//        }

        val radio = when (currentTask.priority) {
            Prio.LOW -> binding.prioLow
            Prio.MEDIUM -> binding.prioMedium
            Prio.HIGH -> binding.prioHigh
        }
        binding.prio.check(radio.id)

        var statusTmp = currentTask.status

        fun setStatusWidgets() {
            binding.editTextStatus.setText(statusTmp.name)
            binding.buttonStatus.text = when (statusTmp) {
                CompletionStatus.OPEN -> "Set status to done"
                CompletionStatus.DONE -> "Reset status to open"
            }
            // also change background
            val bgColor = when (statusTmp) {
                CompletionStatus.OPEN -> Color.WHITE
                CompletionStatus.DONE -> Color.LTGRAY
            }
            binding.root.setBackgroundColor(bgColor)

            // also change name strike through
            binding.editTextTsk.paintFlags = when (statusTmp) {
                CompletionStatus.DONE -> binding.editTextTsk.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                else -> binding.editTextTsk.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            }
        }

        setStatusWidgets()

        binding.buttonStatus.setOnClickListener {
            statusTmp = when (statusTmp) {
                CompletionStatus.OPEN -> CompletionStatus.DONE
                CompletionStatus.DONE -> CompletionStatus.OPEN
            }
            setStatusWidgets()
        }

        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss" )
        val createdString = sdf.format(currentTask.createDate.time).replace(" ", "\u00A0")
        binding.createDate.setText("created: $createdString")
        val modifiedString = sdf.format(currentTask.lastModifiedDate.time).replace(" ", "\u00A0")
        binding.modifiedDate.setText("last modified: $modifiedString")

        binding.buttonCancel.setOnClickListener {
            Toast.makeText(
                this.context,
                "editing canceled ",
                Toast.LENGTH_LONG
            ).show()
            act.supportFragmentManager.popBackStack()
        }

        binding.buttonSave.setOnClickListener {
            val newName = binding.editTextTsk.text.toString().trim()
            if (newName.isBlank() ) {
                Toast.makeText(
                    this.context,
                    "error: task name must not be blank",
                    Toast.LENGTH_LONG
                ).show()

            } else if ((act.appState.tasks.taskCreateIsPending || currentTask.name != newName) && act.appState.tasks.tasksWithNameExisting(newName)) {
                // error if name is not unique
                Toast.makeText(
                    this.context,
                    "error: another task with name $newName already existing",
                    Toast.LENGTH_LONG
                ).show()

            } else {
                val newDesc = binding.editTextDesc.text.toString()
                val catNameSelected = binding.catSpinner.selectedItem as String
                val newCategory = act.appState.categories.categoryForName(catNameSelected)
                val dueDateString = binding.editDueDate.text.toString()
                val newDueDate = if (dueDateString.isNotEmpty()) TDate.fromStorageRep(dueDateString) else null
                val dueTimeString = binding.editDueTime.text.toString()
                val newDueTime = if (dueTimeString.isNotEmpty()) TTime.fromStorageRep(dueTimeString) else null
                val newPrio = when (binding.prio.checkedRadioButtonId) {
                    binding.prioLow.id -> Prio.LOW
                    binding.prioMedium.id -> Prio.MEDIUM
                    binding.prioHigh.id -> Prio.HIGH
                    else -> Prio.LOW
                }

                if (   newName == currentTask.name
                    && newDesc == currentTask.desc
                    && newCategory == currentTask.category
                    && newDueDate == currentTask.dueDate
                    && newDueTime == currentTask.dueTime
                    && newPrio == currentTask.priority
                    && statusTmp == currentTask.status    ) {
                    val message = "no changes, nothing saved"
                    Toast.makeText(
                        this.context,
                        message,
                        Toast.LENGTH_LONG
                    ).show()

                } else {
                    currentTask.name = newName
                    currentTask.desc = binding.editTextDesc.text.toString()
                    currentTask.category = newCategory
                    currentTask.dueDate = newDueDate
                    currentTask.dueTime = newDueTime
                    currentTask.priority = newPrio
                    currentTask.status = statusTmp

                    if (act.appState.tasks.taskCreateIsPending) {
                        act.appState.tasks.addTask(currentTask)
                    } else {
                        // when task was changed, filtering needed
                        act.appState.tasks.onTaskChanged()
                    }
                    act.writeTasks()
                    val message = if (act.appState.tasks.taskCreateIsPending) "task ${currentTask.name} created" else "changes for task ${currentTask.name} saved"
                    Toast.makeText(
                        this.context,
                        message,
                        Toast.LENGTH_LONG
                    ).show()
                }
                act.supportFragmentManager.popBackStack()
            }
        }

        return binding.root
    }
}
