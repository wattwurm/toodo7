package com.wattwurm.toodoo

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wattwurm.toodoo.data.SortCrit
import java.util.*

class SortCriteriaAdapter(val sortOrder: MutableList<SortCrit> ) : RecyclerView.Adapter<SortCriteriaAdapter.SortCritViewHolder>() {

    class SortCritViewHolder(val textView: TextView) : RecyclerView.ViewHolder(textView) {
    }

    override fun getItemCount(): Int {
        //return data.categories.size
        return sortOrder.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SortCritViewHolder {
        // create a new view and wrap it in holder
        val textView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.sort_crit_item, parent, false) as TextView
        return SortCritViewHolder(textView)
    }

    override fun onBindViewHolder(holder: SortCritViewHolder, position: Int) {
        holder.textView.text = sortOrder.get(position).name
    }


    fun onItemMove(fromPosition: Int, toPosition: Int) {
        // directly swap fromPosition: Int, toPosition: Int
        Collections.swap(sortOrder, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
    }
}
