package com.wattwurm.toodoo

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wattwurm.toodoo.data.*
import com.wattwurm.toodoo.databinding.TaskItemRBinding

class TasksAdapter(private val tasks: Tasks) : RecyclerView.Adapter<TasksAdapter.ItemViewHolderR>(){

    class ItemViewHolderR(val binding: TaskItemRBinding) : RecyclerView.ViewHolder(binding.root)

    override fun getItemCount(): Int {
        return tasks.countSelectedTasks
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolderR {
        val binding = TaskItemRBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolderR(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolderR, position: Int) {
        val currentTask = tasks.itemAtPosition(position)
        holder.binding.taskName.text = currentTask.name
        holder.binding.taskName.paintFlags = when (currentTask.status) {
            CompletionStatus.DONE -> holder.binding.taskName.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            else -> holder.binding.taskName.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
        }

        holder.binding.catName.text = currentTask.category.name
        holder.binding.date.text = currentTask.dueDate?.displayInTaskList ?: ""
        holder.binding.timeOrYear.text = currentTask.contentOfTimeOrYear

        val prioResource = when (tasks.itemAtPosition(position).priority) {
            Prio.LOW -> R.drawable.circle_green
            Prio.MEDIUM -> R.drawable.circle_yellow
            Prio.HIGH -> R.drawable.circle_red
        }
        holder.binding.imagePrio.setImageResource(prioResource)

        val bgResource =
            if (currentTask.status == CompletionStatus.DONE) R.drawable.taskbg_completed
            else if (currentTask.isOverdue) R.drawable.taskbg_overdue
            else if (currentTask.dueDate == TDate.today) R.drawable.taskbg_today
            else R.drawable.taskbg_future
        holder.binding.root.setBackgroundResource(bgResource)

        holder.binding.root.setOnCreateContextMenuListener { contextMenu, _, _ ->
            tasks.setCurrentPosition(position)
        }
    }

}

val TDate.displayInTaskList get() =
    "${this.dayOfWeek.shortName}, ${Month.values()[this.month.toInt()-1].shortName} ${"00${this.day}".takeLast(2)}"

val Task.contentOfTimeOrYear : String get() {
    this.dueDate?.let {
        if(it.isMoreThanOneYearFromToday()) {
            return it.year.toString()
        }
    }
    return this.dueTime?.toStorageRep() ?: ""
}
