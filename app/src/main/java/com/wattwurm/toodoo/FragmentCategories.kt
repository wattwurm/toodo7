package com.wattwurm.toodoo

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.wattwurm.toodoo.data.ListMode
import com.wattwurm.toodoo.databinding.DialogCatBinding
import com.wattwurm.toodoo.databinding.FragmentCategoriesBinding

/**
 */
class FragmentCategories : Fragment() {

    private lateinit var act: MainActivity
    private lateinit var binding: FragmentCategoriesBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.i("DEBUG", "FragmentCategories.onCreateView")

        binding = FragmentCategoriesBinding.inflate(inflater, container, false)
        act = this.activity as MainActivity

        binding.categoriesOverview.setText(statusText())

        val catsRecyclerView = binding.catList
        catsRecyclerView.addItemDecoration(
            DividerItemDecoration(
                act,
                DividerItemDecoration.VERTICAL
            )
        )

        val catsAdapter = CategoriesAdapter(act.appState)
        catsRecyclerView.adapter = catsAdapter
        // catsAdapter.notifyDataSetChanged()  // not sure why this is needed, work anyway

        registerForContextMenu(catsRecyclerView)

        // needed because onCreateOptionsMenu must be called
        setHasOptionsMenu(true)

        return binding.root
        //return inflater.inflate(R.layout.fragment_categories, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_cat, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.new_cat -> {
                createDialogForCatCreate()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        // menu.setHeaderTitle("Select what to do with category")
        act.menuInflater.inflate(R.menu.menu_cat_ctxt, menu)
        if(act.appState.tasks.numberOfTasksForCategory(act.appState.categories.currentItem) > 0) {
            menu.findItem(R.id.deleteCategory).setVisible(false)
        } else {
            menu.findItem(R.id.displayTasks).setVisible(false)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.changeCatName -> {
                createDialogForCatReName()
                true
            }
            R.id.deleteCategory -> {
                deleteCat()
                true
            }
            R.id.displayTasks -> {
                displayTasks()
                true
            }
            R.id.createCategory -> {
                createDialogForCatCreate()
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }


    private fun createDialogForCatReName() {
        //val position = act.appState.categories.currentCatPos
        val oldName = act.appState.categories.currentItem.name

        val dialogCatBinding = DialogCatBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this.context)
        builder
            .setMessage("Rename:")
            .setView(dialogCatBinding.root)
            .setPositiveButton("Save") { dialog, id -> }
            .setNegativeButton("Cancel") { dialog, id -> } // User cancelled the dialog
        val dialog = builder.create()
        dialog.show()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { view ->
            val newName = dialogCatBinding.editTextCatName.text.toString().trim()
            if (newName.isBlank()) {
                // show error message, do not close
                dialogCatBinding.catMessage.text = "Name must not be blank!"
            } else if (newName == oldName) {
                dialog.dismiss()
                // check if name already existing for some OTHER category (ignore case)
            } else if (act.appState.categories.containsName(newName) && (newName.toLowerCase() != oldName.toLowerCase())) {
                // show error message, do not close
                dialogCatBinding.catMessage.text = "Category with this name already existing!"
            } else {
                act.appState.categories.renameCurrentItem(newName)
                binding.catList.adapter?.notifyDataSetChanged()
                dialog.dismiss()
                act.writeCategories()
            }
        }
        // display old name in EditText so it is easier to change
        dialogCatBinding.editTextCatName.setText(oldName)
        dialogCatBinding.editTextCatName.setSelection(dialogCatBinding.editTextCatName.text.length) // moves cursor to end of text
    }

    private fun createDialogForCatCreate() {
        val dialogCatBinding = DialogCatBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this.context)
        builder
            .setMessage("New category name:")
            .setView(dialogCatBinding.root)
            .setPositiveButton("Save") { dialog, id -> } // needed to create Save button
            .setNegativeButton("Cancel") { dialog, id -> } // User cancelled the dialog
        val dialog = builder.create()
        dialog.show()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { view ->
            val newName = dialogCatBinding.editTextCatName.text.toString().trim()
            if (newName.isBlank()) {
                // show error message, do not close
                dialogCatBinding.catMessage.text = "Name must not be blank!"
            } else if (act.appState.categories.containsName(newName)) {
                // check if name already existing
                // show error message, do not close
                dialogCatBinding.catMessage.text = "Category with this name already existing!"
            } else {
                act.appState.categories.addCategoryWithName(newName)
                binding.catList.adapter?.notifyDataSetChanged()
                binding.categoriesOverview.setText(statusText())
                dialog.dismiss()
                act.writeCategories()
            }
        }
    }


    private fun deleteCat() {
        val currentCategory = act.appState.categories.currentItem
        val catName = currentCategory.name

        if (act.appState.tasks.tasksExistingForCategory(currentCategory)) {
            // check if category has tasks
            val builder = AlertDialog.Builder(this.context)
            builder
                .setMessage("Category $catName cannot be deleted, still has tasks")
                .setPositiveButton("OK") { dialog, id -> }
            val dialog = builder.create()
            dialog.show()
            // now it is possible to delete the very last category
            // then, at next start of toodoo, example categories and tasks will be generated
//        } else if (act.appState.categories.countItems < 2) {
//            // check if there is only one category left
//            val builder = AlertDialog.Builder(this.context)
//            builder
//                .setMessage("Last category cannot be deleted")
//                .setPositiveButton("OK") { dialog, id -> }
//            val dialog = builder.create()
//            dialog.show()
        } else if (act.appState.tasks.filters.categoryIsUsedInFilter(currentCategory)) {
            // check if category is used as filter
            val builder = AlertDialog.Builder(this.context)
            builder
                .setMessage("Category $catName cannot be deleted, used as filter")
                .setPositiveButton("OK") { dialog, id -> }
            val dialog = builder.create()
            dialog.show()
        } else {
            act.appState.categories.removeCurrentItem()
            // binding.catList.adapter?.notifyItemRemoved(position)  // this is not sufficient, will not update the positions in the adapter!!!
            binding.catList.adapter?.notifyDataSetChanged()
            Toast.makeText(
                this.context,
                "category $catName deleted",
                Toast.LENGTH_LONG
            ).show()
            binding.categoriesOverview.setText(statusText())
            act.writeCategories()
        }
    }

    private fun displayTasks() {
        val currentCategory = act.appState.categories.currentItem
        Toast.makeText(this.context, "display tasks for category ${currentCategory.name}", Toast.LENGTH_LONG).show()

        // go back to FragmentTaskList
        // todo - is there a way to pop the call stack more efficiently?  going to FragmentTaskList actually is not needed
        act.supportFragmentManager.popBackStackImmediate()

        // prepare going to second level list
        act.appState.tasks.listModePending = ListMode.SINGLE_CATEGORY
        act.appState.tasks.filterCategory = currentCategory
        act.showTaskListSecondLevel()
    }

    private fun statusText(): String {
        val numberOfCategories = act.appState.categories.countItems
        return if (numberOfCategories != 1) {
            "${numberOfCategories} categories"
        } else {
            "1 category"
        }
    }
}