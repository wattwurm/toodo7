package com.wattwurm.toodoo

import android.view.View
import androidx.recyclerview.widget.RecyclerView

// helper extension function
// trick from https://gist.github.com/arcadefire/1e3a95314fdbdd78fb211b099d6ec9da

fun RecyclerView.addOnItemLongClickListener(onClick: (position: Int, view: View) -> Unit) {
    this.addOnChildAttachStateChangeListener(
        object : RecyclerView.OnChildAttachStateChangeListener {

            override fun onChildViewDetachedFromWindow(view: View) {
                view.setOnLongClickListener(null)
            }

            override fun onChildViewAttachedToWindow(view: View) {
                view.setOnLongClickListener {
                    val holder = getChildViewHolder(view)
                    onClick(holder.adapterPosition, view)
                    true
                }
            }
        }
    )
}

fun RecyclerView.addOnItemClickListener(onClick: (position: Int, view: View) -> Unit) {

    this.addOnChildAttachStateChangeListener(
        object : RecyclerView.OnChildAttachStateChangeListener {

            override fun onChildViewDetachedFromWindow(view: View) {
                view.setOnClickListener (null)
            }

            override fun onChildViewAttachedToWindow(view: View) {
                view.setOnClickListener {
                    val holder = getChildViewHolder(view)
                    onClick(holder.adapterPosition, view)
                }
            }
        }
    )
}
