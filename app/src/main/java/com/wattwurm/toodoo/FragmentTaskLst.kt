package com.wattwurm.toodoo

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.wattwurm.toodoo.data.*
import com.wattwurm.toodoo.databinding.DialogSearchBinding
import com.wattwurm.toodoo.databinding.FragmentTaskLstBinding

class FragmentTaskLst : Fragment() {

    private val requestCodeImport= 123
    private val requestCodeImportFromTML = 456

    private lateinit var act: MainActivity
    private lateinit var binding: FragmentTaskLstBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        Log.i("DEBUG", "FragmentTaskLst.onCreateView")
        act = this.activity as MainActivity

        Log.i("DEBUG", "ListMode old: ${act.appState.tasks.listMode} ")
        Log.i("DEBUG", "ListMode new: ${act.appState.tasks.listModePending} ")
        // there may be a pending list mode, so first reset tasks in list
        act.appState.tasks.resetTaskListByListMode()

        // Inflate the layout for this fragment
        binding = FragmentTaskLstBinding.inflate(inflater, container, false)
        binding.taskLstHeader.setText(textHeader())
        binding.taskLstNumTasks.setText(textNumTasks())

        val itemsRecyclerView = binding.taskListInFragment
        itemsRecyclerView.addItemDecoration(
            DividerItemDecoration(
                act,
                DividerItemDecoration.VERTICAL
            )
        )

        val recyclerViewAdapter = TasksAdapter(act.appState.tasks)
        itemsRecyclerView.adapter = recyclerViewAdapter
        recyclerViewAdapter.notifyDataSetChanged()

        itemsRecyclerView.addOnItemClickListener { position: Int, view: View ->
            act.appState.tasks.setCurrentPosition(position)
            act.showTaskDetail()
        }

        setHasOptionsMenu(true)
        registerForContextMenu(binding.taskListInFragment)  // needed for the context menu

        // handle back pressed for lists on second level, i.e. when list displays search results or tasks for one category
        act.onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(act.appState.tasks.listMode.isSecondLevelList) {
            override fun handleOnBackPressed() {
                act.appState.tasks.listModePending = ListMode.FILTERED_LIST
//                act.onBackPressed()       //  does NOT pop fragment from back stack, makes app freeze  :-(
//                requireActivity().onBackPressed()  // does NOT pop fragment from back stack either
                act.supportFragmentManager.popBackStack()
            }
        })

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_lst, menu)
        if(act.appState.tasks.listMode.isSecondLevelList) {
            menu.findItem(R.id.search).setVisible(false)
            menu.findItem(R.id.setFilter).setVisible(false)
            menu.findItem(R.id.setSortCrit).setVisible(false)
            menu.findItem(R.id.manageCategories).setVisible(false)
            menu.findItem(R.id.exportData).setVisible(false)
            menu.findItem(R.id.importData).setVisible(false)
//          menu.findItem(R.id.importTMLite).setVisible(false)
            if(act.appState.tasks.listMode == ListMode.STRING_SEARCH) {
                menu.findItem(R.id.new_task).setVisible(false)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.new_task -> {
                act.appState.tasks.setCreateItemPending()
                act.showTaskDetail()
                true
            }
            R.id.search -> {
                createDialogForSearch()
                true
            }
            R.id.setFilter -> {
                act.showFilters()
                true
            }
            R.id.setSortCrit -> {
                act.showSortCrit()
                true
            }
            R.id.exportData -> {
                act.checkAndWriteTasksToExt()
                true
            }
            R.id.importData -> {
                startIntentForFileRead(requestCodeImport)
                true
            }
//            R.id.importTMLite -> {
//                startIntentForFileRead(requestCodeImportFromTML)
//                true
//            }
            R.id.manageCategories -> {
                act.showCatList()
                true
            }
            R.id.about -> {
                act.createDialogAbout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        // menu.setHeaderTitle("Select what to do with task")
        act.menuInflater.inflate(R.menu.menu_tsk_ctxt, menu)
        when (act.appState.tasks.currentTask.status) {
            CompletionStatus.OPEN -> {
                menu.findItem(R.id.resetOpen).setVisible(false)
                menu.findItem(R.id.deleteTask).setVisible(false)
            }
            CompletionStatus.DONE -> {
                menu.findItem(R.id.setDone).setVisible(false)
            }
        }
        if(act.appState.tasks.currentTask.dueDate == TDate.today) {
            menu.findItem(R.id.setDueDateToday).setVisible(false)
        }
        if(act.appState.tasks.currentTask.dueDate == TDate.tomorrow) {
            menu.findItem(R.id.setDueDateTomorrow).setVisible(false)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val taskName = act.appState.tasks.currentTask.name
        return when (item.itemId) {
            R.id.editTask -> {
                act.showTaskDetail()
                true
            }
            R.id.deleteTask -> {
                act.appState.tasks.removeCurrentItem()
                // binding.taskListInFragment.adapter?.notifyItemRemoved(position)
                // this is not sufficient, will not update the positions in the adapter!!!
                binding.taskListInFragment.adapter?.notifyDataSetChanged()
                binding.taskLstNumTasks.setText(textNumTasks())
                Toast.makeText(
                    this.context,
                    "task $taskName deleted",
                    Toast.LENGTH_LONG
                ).show()
                act.writeTasks()
                true
            }
            R.id.setDueDateToday -> {
                act.appState.tasks.currentTask.dueDate = TDate.today
                act.appState.tasks.onTaskChanged()
                binding.taskListInFragment.adapter?.notifyDataSetChanged()
                binding.taskLstNumTasks.setText(textNumTasks())
                Toast.makeText(
                    this.context,
                    "changes for task $taskName saved",
                    Toast.LENGTH_LONG
                ).show()
                act.writeTasks()
                true
            }
            R.id.setDueDateTomorrow -> {
                act.appState.tasks.currentTask.dueDate = TDate.tomorrow
                act.appState.tasks.onTaskChanged()
                binding.taskListInFragment.adapter?.notifyDataSetChanged()
                binding.taskLstNumTasks.setText(textNumTasks())
                Toast.makeText(
                    this.context,
                    "changes for task $taskName saved",
                    Toast.LENGTH_LONG
                ).show()
                act.writeTasks()
                true
            }
            R.id.setDone -> {
                act.appState.tasks.currentTask.status = CompletionStatus.DONE
                act.appState.tasks.onTaskChanged()
                binding.taskListInFragment.adapter?.notifyDataSetChanged()
                binding.taskLstNumTasks.setText(textNumTasks())
                Toast.makeText(
                    this.context,
                    "changes for task $taskName saved",
                    Toast.LENGTH_LONG
                ).show()
                act.writeTasks()
                true
            }
            R.id.resetOpen -> {
                act.appState.tasks.currentTask.status = CompletionStatus.OPEN
                act.appState.tasks.onTaskChanged()
                binding.taskListInFragment.adapter?.notifyDataSetChanged()
                binding.taskLstNumTasks.setText(textNumTasks())
                Toast.makeText(
                    this.context,
                    "changes for task $taskName saved",
                    Toast.LENGTH_LONG
                ).show()
                act.writeTasks()
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == requestCodeImport) {
            if (resultCode == Activity.RESULT_OK) {
                val selectedfile: Uri? = data?.data    //The uri with the location of the file
                Log.i("DEBUG", "URI is $selectedfile")
                if (selectedfile != null) {
                    val inputStream = act.contentResolver.openInputStream(selectedfile)
                    if (inputStream != null) {
                        try {
                            act.appState.readAllDataFromStream(inputStream)
                            binding.taskListInFragment.adapter?.notifyDataSetChanged()
                            binding.taskLstHeader.setText(textHeader())
                            binding.taskLstNumTasks.setText(textNumTasks())
                            act.writeCategories()
                            act.writeTasks()
                            act.writeFilters()
                            act.writeSortCrit()
                            Toast.makeText(this.context, "imported ${act.appState.categories.countItems} categories and ${act.appState.tasks.countAllTasks} tasks ", Toast.LENGTH_LONG).show()
                        } catch (e: Exception) {
                            Toast.makeText(this.context, "error importing data $e", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        Log.i("DEBUG", "selected file $selectedfile could not be resolved to inputstream")
                    }
                } else {
                    Log.i("DEBUG", "selected file $selectedfile does not exist")
                }
            } else {
                Log.i("DEBUG", "onActivityResult requestCode $requestCode (import data) got resultCode $resultCode")
            }
        } else if (requestCode == requestCodeImportFromTML) {
            if (resultCode == Activity.RESULT_OK) {
                val selectedfile: Uri? = data?.data    //The uri with the location of the file
                Log.i("DEBUG", "URI is $selectedfile")
                if (selectedfile != null) {
                    val inputStream = act.contentResolver.openInputStream(selectedfile)
                    if (inputStream != null) {
                        try {
                            act.appState.readTMLiteDataFromStream(inputStream)
                            binding.taskListInFragment.adapter?.notifyDataSetChanged()
                            binding.taskLstHeader.setText(textHeader())
                            binding.taskLstNumTasks.setText(textNumTasks())
                            // todo for test inactivated
//                            act.writeCategories()
//                            act.writeTasks()
//                            act.writeFilters()
//                            act.writeSortCrit()
                            Toast.makeText(this.context, "imported ${act.appState.categories.countItems} categories and ${act.appState.tasks.countAllTasks} tasks ", Toast.LENGTH_LONG).show()
                        } catch (e: Exception) {
                            Toast.makeText(this.context, "error importing TML data $e", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        Log.i("DEBUG", "selected file $selectedfile could not be resolved to inputstream")
                    }
                } else {
                    Log.i("DEBUG", "selected file $selectedfile does not exist")
                }
            } else {
                Log.i("DEBUG", "onActivityResult requestCode $requestCode (import data from TML) got resultCode $resultCode")
            }
        }
    }

    private fun textHeader(): String {
        return when (act.appState.tasks.listMode) {
            ListMode.FILTERED_LIST -> act.appState.tasks.filters.displayInOverview.let { if (it.isNotBlank()) "tasks filtered by $it" else "all tasks" }
            ListMode.STRING_SEARCH -> "search result for: ${act.appState.tasks.searchCrit.sstring}"
            ListMode.SINGLE_CATEGORY -> "tasks for category: ${act.appState.tasks.filterCategory.name}"
        }
    }

    private fun textNumTasks(): String {
        val numberAllTasks = act.appState.tasks.countAllTasks
        val numberSelTasks = act.appState.tasks.countSelectedTasks
        return "${numberSelTasks} of ${numberAllTasks}"
    }

    private fun startIntentForFileRead(requestCode: Int) {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.setType("text/xml")
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(intent, requestCode)
    }

    private fun createDialogForSearch() {
        val dialogSearchBinding = DialogSearchBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this.context)
        builder
            .setMessage("Search for:")
            .setView(dialogSearchBinding.root)
            .setPositiveButton("Search") { dialog, id -> }
            .setNegativeButton("Cancel") { dialog, id -> }
        val dialog = builder.create()
        dialog.show()
        dialogSearchBinding.searchText.setText(act.appState.tasks.searchCrit.sstring)
        dialogSearchBinding.searchText.setSelection(dialogSearchBinding.searchText.text.length) // moves cursor to end of text

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { view ->
            val searchString = dialogSearchBinding.searchText.text.toString()
            if (searchString.isBlank()) {
                // show error message, and do not close
                dialogSearchBinding.searchMessage.text = "Search string must not be blank!"
            } else {
                val option = when (dialogSearchBinding.nameOrDesc.checkedRadioButtonId) {
                    dialogSearchBinding.nameOnly.id -> SearchOption.NAME_ONLY
                    dialogSearchBinding.descOnly.id -> SearchOption.DESC_ONLY
                    else -> SearchOption.NAME_AND_DESC
                }
                dialog.dismiss()

                // prepare showing results of string search, i.e. second level list
                act.appState.tasks.listModePending = ListMode.STRING_SEARCH
                act.appState.tasks.searchCrit = SearchCrit(searchString, option)
                act.showTaskListSecondLevel()
            }
        }
    }
}
