package com.wattwurm.toodoo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.wattwurm.toodoo.databinding.FragmentSortcriteriaBinding


/**
 */
class FragmentSortCriteria : Fragment() {

    private lateinit var act: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentSortcriteriaBinding.inflate(inflater, container, false)
        act = this.activity as MainActivity

        val sortRecyclerView = binding.sortCritList
        sortRecyclerView.addItemDecoration(
            DividerItemDecoration(act, DividerItemDecoration.VERTICAL)
        )

        val sortOrderNew = act.appState.tasks.currentSortOrder.toMutableList()  // create copy that can be mutated in this fragment
        val adapter = SortCriteriaAdapter(sortOrderNew)
        sortRecyclerView.adapter = adapter
        adapter.notifyDataSetChanged()  // not sure if this is needed, would also works without

        val callback = MoveUpDownHelperCallback(adapter)
        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(sortRecyclerView)

        binding.buttonCancel.setOnClickListener {
            act.supportFragmentManager.popBackStack()
        }

        binding.buttonDefault.setOnClickListener {
            sortOrderNew.clear()
            sortOrderNew.addAll(act.appState.tasks.defaultSortOrder)
            adapter.notifyDataSetChanged()
            Toast.makeText(this.context, "sort order reset to default", Toast.LENGTH_LONG).show()
        }

        binding.buttonBackFromSortCrit.setOnClickListener {
            // check if sort order was changed, otherwise sorting and storing not necessary
            if (sortOrderNew != act.appState.tasks.currentSortOrder) {
                act.appState.tasks.setSortOrder(sortOrderNew)
                act.writeSortCrit()
                Log.i("DEBUG", "FragmentSortCriteria new sort criteria were applied and saved")
            } else {
                Log.i("DEBUG", "FragmentSortCriteria sort criteria were not changed")
            }
            act.supportFragmentManager.popBackStack()
        }

        return binding.root
    }

}



class MoveUpDownHelperCallback(private val mAdapter: SortCriteriaAdapter) : ItemTouchHelper.Callback() {

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: ViewHolder): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = 0  // ItemTouchHelper.START or ItemTouchHelper.END  // not needed
        return makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: ViewHolder,
        target: ViewHolder
    ): Boolean {
        mAdapter.onItemMove(viewHolder.adapterPosition, target.adapterPosition)
        // viewHolder.itemView.tooltipText = ""  // needs sdkVerson 26, does not work for sdkVersion 23
        return true
    }

    override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
        // do nothing, swipe not supported
    }

}