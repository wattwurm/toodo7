package com.wattwurm.toodoo

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wattwurm.toodoo.data.FilterAll
import com.wattwurm.toodoo.data.SingleFilter
import com.wattwurm.toodoo.data.SingleFilter1
import com.wattwurm.toodoo.databinding.FilteroptionItemRBinding

class SingleFilterAdapter<T>(private val options: List<T>, var currentFilter: SingleFilter<T>) : RecyclerView.Adapter<SingleFilterAdapter.FilterOptionHolder>(){

    class FilterOptionHolder(val binding: FilteroptionItemRBinding) : RecyclerView.ViewHolder(binding.root)

    override fun getItemCount(): Int {
        return options.size + 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterOptionHolder {
        val binding = FilteroptionItemRBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FilterOptionHolder(binding)
    }

    override fun onBindViewHolder(holder: FilterOptionHolder, position: Int) {

        if (position == 0) {
            // for first option <all> add a line to separate it from other options
            holder.binding.root.setBackgroundResource(R.drawable.shape_line_bottom)
            holder.binding.filterItemText.text = FilterAll.display
            holder.binding.filterItemRadio.isChecked = (currentFilter == FilterAll)
            Log.i("DEBUG", "SingleFilterAdapter onBindViewHolder, position $position is ${FilterAll.display} ")
        } else {
            holder.binding.root.setBackgroundResource(0)
            val currentItem = options[position-1]
            val filter1 = currentFilter
            holder.binding.filterItemText.text = currentItem.toString()
            holder.binding.filterItemRadio.isChecked = filter1 is SingleFilter1 && filter1.option == currentItem
            Log.i("DEBUG", "SingleFilterAdapter onBindViewHolder, content on position $position is ${currentItem} ")
        }

        holder.binding.filterItemRadio.setOnClickListener { view ->
            val filterOld = currentFilter
            currentFilter = if (position == 0) FilterAll
            else SingleFilter1(options[position-1])
            if (filterOld != currentFilter) {
                this.notifyDataSetChanged()
            }
        }
    }
}
