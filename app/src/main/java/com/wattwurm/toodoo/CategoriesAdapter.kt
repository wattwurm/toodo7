package com.wattwurm.toodoo

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.wattwurm.toodoo.data.AppState
import com.wattwurm.toodoo.databinding.CatItemRBinding

class CategoriesAdapter(private val data: AppState) : RecyclerView.Adapter<CategoriesAdapter.CatViewHolder>(){

    class CatViewHolder(val binding: CatItemRBinding) : RecyclerView.ViewHolder(binding.root)

    override fun getItemCount(): Int {
        //return data.categories.size
        return data.categories.countItems
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatViewHolder {
        val binding = CatItemRBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CatViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {
        val cat = data.categories.itemAtPosition(position)
        val numTasks = data.tasks.numberOfTasksForCategory(cat)
        holder.binding.catName.text = cat.name
        holder.binding.numTasks.text = numTasks.toString()

        holder.binding.root.setOnCreateContextMenuListener { _, _, _ ->
            data.categories.setCurrentPosition(position)
        }
    }
}
