package com.wattwurm.toodoo.data

import java.lang.Exception

class Categories {
    private val catList = mutableListOf<Category>()
    private var currentCatPos = -1

    private var nextId = 0
    private fun getNextId() : Int {
        nextId++
        return nextId
    }
    fun resetNextId() {
        nextId = 0
    }
    private fun resetNextIdFromCategoryList () {
        nextId = catList.map { it.id }.max() ?: 0
    }

    fun createNewCategory(name: String): Category {
        return Category(getNextId(), name)
    }

    fun setCurrentPosition(pos: Int) {
        currentCatPos = pos
    }

    fun categoryForId(id: Int): Category {
        for (cat in catList ) {
            if (cat.id == id) return cat
        }
        throw Exception("category with id $id not existing")
    }

    fun categoryForName(name: String): Category {
        for (cat in catList ) {
            if (cat.name == name) return cat
        }
        throw Exception("category with name $name not existing")
    }

    fun containsName(name: String): Boolean {
        for (cat in catList ) {
            if (cat.name.toLowerCase() == name.toLowerCase()) return true
        }
        return false
    }

    val countItems get() = catList.size

    fun itemAtPosition(pos: Int): Category {
        return catList[pos]
    }

    // precondition: currentCatPos > -1
    val currentItem: Category
        get() {
            return catList[currentCatPos]
        }

    fun removeCurrentItem() {
        catList.removeAt(currentCatPos)
        // remove of category is safe:
        //  before deleting, FragmentCategories checks that category is not used in tasks or filters
    }

    fun addCategoryWithName(name: String) {
        catList.add(this.createNewCategory(name))
        sortCategories()
    }

    fun renameCurrentItem(name: String) {
        val cat = catList[currentCatPos]
        cat.name = name
        sortCategories()
    }

    private fun sortCategories() {
        catList.sort()
    }

    fun setCategoriesFromStorage(categories: List<Category>) {
        with(catList) {
            clear()
            addAll(categories)
            sortCategories()
        }
        resetNextIdFromCategoryList()
    }

    val iterator get() = catList.iterator()

    val allCategoryNames get() = catList.map { it.name }

}


class Category (val id: Int, var name: String): Comparable<Category> {

    // toString determines what is displayed by ArrayAdapter in FragmentDetail
    override fun toString(): String {
        return name
    }

    fun toStorageRep(): String {
        return "${id}\t${name}"
    }

    override fun compareTo(other: Category): Int {
        return this.name.toLowerCase().compareTo(other.name.toLowerCase())
    }
}
