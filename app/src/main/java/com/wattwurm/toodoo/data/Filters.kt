package com.wattwurm.toodoo.data

class Filters {

    lateinit var completionFilter : SingleFilter<CompletionStatus>
    lateinit var dueDateFilter  : SingleFilter<DateFilter>
    lateinit var prioFilter : MultipleFilter<Prio>
    lateinit var catFilter  : MultipleFilter<Category>

    init {
        resetToDefault()
    }

    fun categoryIsUsedInFilter(cat: Category) : Boolean {
        catFilter.let {
            return (it is MultipleFilterM && it.options.contains(cat))
        }
    }

    fun applyToTask(task: Task) : Boolean {
        dueDateFilter.let {
            if (it is SingleFilter1) {
                val date = task.dueDate
                if (date == null) {
                    if (it.option != DateFilter.NO_DATE ) return false
                } else {
                    when (it.option) {
                        DateFilter.OVERDUE -> if(!task.isOverdue) return false
                        DateFilter.TODAY -> if(date != TDate.today) return false
                        DateFilter.TOMORROW -> if(date != TDate.tomorrow) return false
                        DateFilter.NEXT_3_DAYS -> {
                            if (date.isLessThan(TDate.today)) return false
                            if (TDate.today.addDays(3).isLessThan(date)) return false
                        }
                        DateFilter.CURRENT_WEEK -> if(!(date.isInCurrentWeek())) return false
                        DateFilter.NEXT_WEEK -> if(!(date.isInNextWeek())) return false
                        DateFilter.IN_MORE_THAN_4_WEEKS -> {
                            if (date.isLessThan(TDate.today.addDays(28))) return false
                        }
                        DateFilter.NO_DATE -> return false
                    }
                }
            }
        }
        completionFilter.let {
            if (it is SingleFilter1) {
                if(task.status != it.option) return false
            }
        }
        prioFilter.let {
            if (it is MultipleFilterM) {
                if(!(it.options.contains(task.priority))) return false
            }
        }
        catFilter.let {
            if (it is MultipleFilterM) {
                if(!(it.options.contains(task.category))) return false
            }
        }
        return true
    }

    val displayInOverview: String get() {
        fun combine (str1: String, str2: String) = if (str1 == "") str2 else "${str1} & ${str2}"
        var result = ""
        completionFilter.let { if(it is SingleFilter1) result = combine(result, it.displayInOverview) }
        dueDateFilter.let { if(it is SingleFilter1) result = combine(result, it.displayInOverview) }
        prioFilter.let { if(it is MultipleFilterM) result = combine(result, it.displayInOverview) }
        catFilter.let { if(it is MultipleFilterM) result = combine(result, it.displayInOverview) }
        return result
    }

    override fun equals(other: Any?): Boolean {
        return if (other is Filters)
            this.completionFilter == other.completionFilter
                    && this.dueDateFilter == other.dueDateFilter
                    && this.prioFilter == other.prioFilter
                    && this.catFilter == other.catFilter
        else false
    }

    override fun hashCode(): Int {
        return ((completionFilter.hashCode() * 31 + dueDateFilter.hashCode()) * 31 + prioFilter.hashCode()) * 31 + catFilter.hashCode()
    }

    fun copy(): Filters {
        val copy = Filters()
        copy.completionFilter = this.completionFilter
        copy.dueDateFilter = this.dueDateFilter
        copy.prioFilter = this.prioFilter
        copy.catFilter = this.catFilter
        return copy
    }

    fun resetToDefault() {
        completionFilter = FilterAll
        dueDateFilter  = FilterAll
        prioFilter = FilterAll
        catFilter = FilterAll
    }

}

enum class DateFilter {
    OVERDUE, TODAY, TOMORROW, NEXT_3_DAYS, CURRENT_WEEK, NEXT_WEEK, IN_MORE_THAN_4_WEEKS, NO_DATE;
}

interface SingleFilter<out T> {  // out is needed here, otherwise FilterAll does not count as a subtype of SingleFilter<out T>
    val display : String
}

data class SingleFilter1<T> (val option: T) : SingleFilter<T> {
    override val display: String get() = option.toString()
    val displayInOverview: String get() = option.toString()
}

interface MultipleFilter<out T> {
    val display : String
}

data class MultipleFilterM<T: Comparable<T>> (val options: List<T>) : MultipleFilter<T> {
    override val display: String get() = options.toString()
    private fun combine (str1: String, str2: String) = if (str1 == "") str2 else "${str1}|${str2}"
    val displayInOverview: String get() = options.fold ("") { e1, e2 -> combine(e1, e2.toString()) }
}

object FilterAll : SingleFilter<Nothing>, MultipleFilter<Nothing> {  // type Nothing is needed here, otherwise FilterAll does not count as a subtype of SingleFilter and MultipleFilter
    override val display: String get() = "[ ALL ]"
}
