package com.wattwurm.toodoo.data

import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Comparator

class Tasks {

    private val allTasks = mutableListOf<Task>()
    private val selectedTasks = mutableListOf<Task>()
    private var currentItemPos = -1

    private var nextId = 0
    fun getNextId() : Int {
        nextId++
        return nextId
    }
    fun resetNextId() {
        nextId = 0
    }
    private fun resetNextIdFromTasks () {
        nextId = allTasks.map { it.id }.max() ?: 0
    }

    var filters = Filters()

    // initially sort by Completed, DueDate, Prio, Category, Name
    val defaultSortOrder = listOf(
        SortCrit.Completed,
        SortCrit.DueDate,
        SortCrit.Priority,
        SortCrit.Category,
        SortCrit.Name
    )

    val currentSortOrder = defaultSortOrder.toMutableList()

    private val taskComparator = Comparator<Task> { t1, t2 ->
        fun taskCompare(t1: Task, t2: Task): Int {
            currentSortOrder.forEach { sortCrit ->
                sortCrit.compare(t1, t2).let { if(it != 0) return it }
            }
            return 0
        }
        taskCompare(t1,t2)
    }

    var listMode = ListMode.FILTERED_LIST
    var listModePending = ListMode.FILTERED_LIST
    lateinit var filterCategory : Category
    var searchCrit = SearchCrit("", SearchOption.NAME_AND_DESC)

    val countAllTasks get() = allTasks.size
    val countSelectedTasks get() = selectedTasks.size

    val taskCreateIsPending get() = currentItemPos == -1

    // precondition: currentCatPos > -1
    val currentTask: Task
        get() {
            return selectedTasks[currentItemPos]
        }

    fun itemAtPosition(pos: Int): Task {
        return selectedTasks[pos]
    }

    val iterator get() = allTasks.iterator()

    fun setCurrentPosition(pos: Int) {
        currentItemPos = pos
    }

    fun setCreateItemPending() {
        currentItemPos = -1
    }

    fun addTask(task: Task) {
        task.id = getNextId()
        allTasks.add(task)
        if(criteriaApplyToTask(task)) {
            // first check if current criteria apply
            selectedTasks.add(task)
            sortSelected()
        }
    }

    fun setTasksFromStorage(taskList: List<Task>) {
        allTasks.clear()
        allTasks.addAll(taskList)
        applyFilters()
        resetNextIdFromTasks()
    }

    fun removeCurrentItem() {
        val current = this.currentTask
        selectedTasks.removeAt(currentItemPos)
        allTasks.remove(current)
    }

    fun tasksExistingForCategory(cat: Category) = allTasks.any { it.category == cat }

    fun tasksWithNameExisting(name: String) = allTasks.any { it.name == name }

    fun numberOfTasksForCategory(cat: Category) = allTasks.count { it.category == cat }

    // different criteria needed, depending on list mode
    private fun criteriaApplyToTask(task: Task) : Boolean {
        return when (this.listMode) {
            ListMode.FILTERED_LIST -> filters.applyToTask(task)
            ListMode.STRING_SEARCH -> searchCrit.applyToTask(task)
            ListMode.SINGLE_CATEGORY -> filterCategory == task.category
        }
    }

    fun onTaskChanged() {
        currentTask.lastModifiedDate = Calendar.getInstance()
        if(!criteriaApplyToTask(currentTask)) {
            selectedTasks.removeAt(currentItemPos)
        } else {
            sortSelected()
        }
    }

    fun resetTaskListByListMode() {
        if(listModePending != listMode) {
            listMode = listModePending
            selectedTasks.clear()
            selectedTasks.addAll(allTasks.filter { criteriaApplyToTask(it) })
            sortSelected()
        }
    }

    fun applyFilters() {
        with(selectedTasks) {
            this.clear()
            this.addAll(allTasks.filter { filters.applyToTask(it) } )
        }
        sortSelected()
    }

    private fun sortSelected() {
        selectedTasks.sortWith(taskComparator)
    }

    fun setSortOrder(newOrder: List<SortCrit>) {
        currentSortOrder.clear()
        currentSortOrder.addAll(newOrder)
        sortSelected()
    }

    fun resetSortOrderToDefault() {
        setSortOrder(defaultSortOrder)
    }

    val singleCategoryIfExisting get() = when (this.listMode) {
        ListMode.FILTERED_LIST -> filters.catFilter.let {
            if (it is MultipleFilterM && it.options.size == 1) it.options[0] else null
        }
        ListMode.STRING_SEARCH -> null
        ListMode.SINGLE_CATEGORY -> filterCategory
    }

}

class Task {
    // id is assigned when task read form storage (then it has an id) or created (then it gets nextId)
    var id: Int = -1
    var name: String = ""
    var priority = Prio.LOW
    var status = CompletionStatus.OPEN
    var dueDate: TDate? = null
    var dueTime: TTime? = null
    lateinit var category: Category
    var createDate: Calendar = Calendar.getInstance()
    var lastModifiedDate: Calendar = Calendar.getInstance()
    var desc: String = ""

    companion object {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ" )
    }

    val isOverdue: Boolean get() {
        this.dueDate?.let { date ->
            if(date.isLessThan(TDate.today)) return true
            if(date == TDate.today) {
                this.dueTime?.let { time ->
                    if(time.compareTo(TTime.now) < 0) return true
                }
            }
        }
        return false
    }

    fun toStorageRep(): String {
        val dueDateS = dueDate?.toStorageRep() ?: ""
        val dueTimeS = dueTime?.toStorageRep() ?: ""
        val catIdS = category.id.toString()
        val createDateS = sdf.format(createDate.time)
        return "${id}\t${name}\t${priority.rep}\t${status.symbol}\t${dueDateS}\t${dueTimeS}\t${catIdS}\t${createDateS}"
    }
}

enum class Prio(val rep: Int) {
    LOW(1),
    MEDIUM(2),
    HIGH(3);

    companion object {
        fun fromNumber(num: Int) = when (num) {
            1 -> LOW
            2 -> MEDIUM
            3 -> HIGH
            else -> throw Exception("Prio for number $num not existing")
        }
    }
}

enum class CompletionStatus(val symbol: Char) {
    OPEN('O'),
    DONE('C');

    companion object {
        fun fromSymbol(symbol: Char) = when (symbol) {
            'O' -> OPEN
            'C' -> DONE
            else -> throw Exception("Status for symbol $symbol not existing")
        }
    }
}


data class SearchCrit (val sstring: String, val option: SearchOption) {
    fun applyToTask(task: Task) =
        (option.searchName && task.name.contains(sstring, ignoreCase = true))
                || (option.searchDesc && task.desc.contains(sstring, ignoreCase = true))
}

enum class SearchOption {
    NAME_AND_DESC {
        override val searchName = true
        override val searchDesc = true
    },
    NAME_ONLY {
        override val searchName = true
        override val searchDesc = false
    },
    DESC_ONLY {
        override val searchName = false
        override val searchDesc = true
    };

    abstract val searchName: Boolean
    abstract val searchDesc: Boolean
}

enum class ListMode {
    FILTERED_LIST, STRING_SEARCH, SINGLE_CATEGORY;

    val isSecondLevelList get() = when (this) {
        FILTERED_LIST -> false
        STRING_SEARCH -> true
        SINGLE_CATEGORY -> true
    }
}
