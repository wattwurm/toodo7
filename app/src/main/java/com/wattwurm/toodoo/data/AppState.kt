package com.wattwurm.toodoo.data

import android.util.Log
import android.util.Xml
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import org.xmlpull.v1.XmlSerializer
import java.io.InputStream
import java.io.OutputStream
import java.util.*

class AppState {

    val tasks = Tasks()
    val categories = Categories()

    fun writeAllDataToStream(stream: OutputStream) {
        createSerializerAndWriteData(stream) {serializer ->
            serializer.startTag(null, "root")
            writeCategoriesToSerializer(serializer)
            writeTasksToSerializer(serializer)
            writeFiltersToSerializer(serializer)
            writeSortCritToSerializer(serializer)
            serializer.endTag(null, "root")
        }
    }

    fun writeCategoriesToStream(stream: OutputStream) {
        createSerializerAndWriteData(stream) { serializer ->
            writeCategoriesToSerializer(serializer)
        }
    }

    private fun writeCategoriesToSerializer(serializer: XmlSerializer) {
        var counter = 0
        with(serializer) {
            startTag(null, "categories")
            for (category in categories.iterator) {
                startTag(null, "category")
                attribute(null, "id", category.id.toString())
                attribute(null, "name", category.name)
                endTag(null, "category")
                counter++
            }
            endTag(null, "categories")
        }
        Log.i("DEBUG", "done: $counter categories written as xml")
    }

    fun writeTasksToStream(stream: OutputStream) {
        createSerializerAndWriteData(stream) { serializer ->
            writeTasksToSerializer(serializer)
        }
    }

    private fun writeTasksToSerializer(serializer: XmlSerializer) {
        var count = 0
        with (serializer) {
            startTag(null, "tasks")
            for (task in tasks.iterator) {
                startTag(null, "task")
                attribute(null, "id", task.id.toString())
                attribute(null, "name", task.name)
                attribute(null, "prio", task.priority.rep.toString())
                attribute(null, "comp", task.status.symbol.toString())
                task.dueDate?.let { attribute(null, "ddate", it.toStorageRep()) }   // skip if null
                task.dueTime?.let { attribute(null, "dtime", it.toStorageRep()) }   // skip if null
                attribute(null, "cat", task.category.id.toString())
                attribute(null, "cdate", Task.sdf.format(task.createDate.time))
                attribute(null, "mdate", Task.sdf.format(task.lastModifiedDate.time))
                if(task.desc.isNotEmpty()) {  //  tag for description only if not empty
                    startTag(null, "desc")
                    text(task.desc)
                    endTag(null, "desc")
                }
                endTag(null, "task")
                count++
            }
            endTag(null, "tasks")
        }
        Log.i("DEBUG", "done: $count tasks written as xml")
    }

    fun readCategoriesFromStream(inputStream: InputStream) {
        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)
        val categoryList = readCategoriesFromParser(parser)
        this.categories.setCategoriesFromStorage(categoryList)
    }

    private fun readCategoriesFromParser(parser: XmlPullParser) : List<Category> {
        val categoryList = mutableListOf<Category>()
        var eventType = parser.eventType
        var counter = 0
        while (!(eventType == XmlPullParser.END_TAG && parser.name == "categories")) {
            if (eventType == XmlPullParser.START_TAG) {
                if(parser.name == "category") {
                    val catId = parser.getAttributeValue(null, "id").toInt()
                    val catName = parser.getAttributeValue(null, "name")
                    val category = Category(catId, catName)
                    categoryList.add(category)
                    counter++
                }
            }
            eventType = parser.next()
        }
        Log.i("DEBUG", "$counter categories read from xml")
        return categoryList
    }

    fun writeFiltersToStream(stream: OutputStream) {
        createSerializerAndWriteData(stream) { serializer ->
            writeFiltersToSerializer(serializer)
        }
    }

    private fun writeFiltersToSerializer(serializer: XmlSerializer) {
        val filters = tasks.filters
        serializer.startTag(null, "filters")
        writeSingleFilterToSerializer(serializer, SortCrit.Completed.name, filters.completionFilter)
        writeSingleFilterToSerializer(serializer, SortCrit.DueDate.name, filters.dueDateFilter)
        writeMultipleFilterToSerializer(serializer, SortCrit.Priority.name, filters.prioFilter)
        writeMultipleFilterToSerializer(serializer, SortCrit.Category.name, filters.catFilter)
        serializer.endTag(null, "filters")
        Log.i("DEBUG", "done: filters written as xml")
    }

    private fun <T> writeSingleFilterToSerializer(serializer: XmlSerializer, crit: String, filter: SingleFilter<T>) {
        with (serializer) {
            startTag(null, "filter")
            attribute(null, "crit", crit)
            val type = when (filter) {
                is FilterAll -> "all"
                is SingleFilter1 -> "single"
                else -> "dummy"  // not possible, can only be FilterAll or SingleFilter1
            }
            attribute(null, "type", type)
            if (filter is SingleFilter1) {
                startTag(null, "option")
                attribute(null, "name", filter.option.toString())
                endTag(null, "option")
            }
            endTag(null, "filter")
        }
    }

    private fun <T> writeMultipleFilterToSerializer(serializer: XmlSerializer, crit: String, filter: MultipleFilter<T>) {
        with (serializer) {
            startTag(null, "filter")
            attribute(null, "crit", crit)
            val type = when (filter) {
                is FilterAll -> "all"
                is MultipleFilterM -> "mult"
                else -> "dummy"  // not possible, can only be FilterAll or MultipleFilterM
            }
            attribute(null, "type", type)
            if (filter is MultipleFilterM) {
                val options = filter.options
                options.forEach {
                    startTag(null, "option")
                    attribute(null, "name", it.toString())
                    endTag(null, "option")
                }
            }
            endTag(null, "filter")
        }
    }

    fun writeSortCritToStream(stream: OutputStream) {
        createSerializerAndWriteData(stream) { serializer ->
            writeSortCritToSerializer(serializer)
        }
    }

    private fun writeSortCritToSerializer(serializer: XmlSerializer) {
        serializer.startTag(null, "sorts")
        tasks.currentSortOrder.forEachIndexed { index, sortCrit ->
            serializer.startTag(null, "sort")
            serializer.attribute(null, "order", (index+1).toString())
            serializer.attribute(null, "crit", sortCrit.toString())
            serializer.endTag(null, "sort")
        }
        serializer.endTag(null, "sorts")
        Log.i("DEBUG", "done: sort criteria written as xml")
    }

    private fun createSerializerAndWriteData(stream: OutputStream, writeToSerializer: (XmlSerializer) -> Unit ) {
        Log.i("DEBUG", "writing data to stream started ")
        val serializer: XmlSerializer = Xml.newSerializer()
        serializer.setOutput(stream, "UTF-8")
        serializer.startDocument(null, true)
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true)
        writeToSerializer(serializer)
        serializer.endDocument()
        serializer.flush()
        stream.close()
        Log.i("DEBUG", "writing data to stream finished")
    }

    fun readTasksFromStream(inputStream: InputStream) {
        Log.i("DEBUG", "readTasksFromStream start")
        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)
        val taskList = readTasksFromParser(parser)

        // for each task, replace dummy category with matching category
        taskList.forEach { it.category = this.categories.categoryForId(it.category.id) }
        this.tasks.setTasksFromStorage(taskList)
        Log.i("DEBUG", "readTasksFromStream end")
    }

    private fun readTasksFromParser(parser: XmlPullParser) : List<Task> {
        val taskList = mutableListOf<Task>()
        var task = Task()
        var descTag = false
        var counter = 0

        var eventType = parser.eventType
        while (!(eventType == XmlPullParser.END_TAG && parser.name == "tasks")) {
            if (eventType == XmlPullParser.START_TAG) {
                if(parser.name == "task") {
                    task = Task()
                    task.id = parser.getAttributeValue(null, "id").toInt()
                    task.name = parser.getAttributeValue(null, "name")
                    task.priority = Prio.fromNumber(parser.getAttributeValue(null, "prio").toInt())
                    task.status = CompletionStatus.fromSymbol(parser.getAttributeValue(null, "comp").toCharArray()[0])
                    task.dueDate = parser.getAttributeValue(null, "ddate")?.let { TDate.fromStorageRep(it) }
                    task.dueTime = parser.getAttributeValue(null, "dtime")?.let { TTime.fromStorageRep(it) }
                    Task.sdf.parse(parser.getAttributeValue(null, "cdate"))?.let { task.createDate.time = it }
                    Task.sdf.parse(parser.getAttributeValue(null, "mdate"))?.let { task.lastModifiedDate.time = it }
                    task.category = Category(parser.getAttributeValue(null, "cat").toInt(), "")
                } else if (parser.name == "desc") {
                    descTag = true
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if(parser.name == "task") {
                    taskList.add(task)
                    Log.i("DEBUG", "task read from stream: ${task.toStorageRep()}")
                    task.desc.let { if(it.isNotEmpty()) { Log.i("DEBUG", "task desc: ${it}")} }
                    counter++
                } else if (parser.name == "desc") {
                    descTag = false
                }
            } else if (eventType == XmlPullParser.TEXT) {
                if (descTag) {
                    // Log.i("DEBUG", "xml parser text in desc: ${parser.text}")
                    task.desc = parser.text
                }
            }
            eventType = parser.next()
        }
        Log.i("DEBUG", "$counter tasks read from xml")
        return taskList
    }


    fun readFiltersFromStream(inputStream: InputStream) {
        Log.i("DEBUG", "readFiltersFromStream start")
        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)
        val filters = readFiltersFromParser(parser)

        // for catFilter, replace dummy categories with matching category, if necessary
        filters.catFilter.let { catFilter ->
            if(catFilter is MultipleFilterM<Category>) {
                val validCategories = catFilter.options.map { categories.categoryForName(it.name) }
                filters.catFilter = MultipleFilterM(validCategories)
            }
        }
        tasks.filters = filters
        tasks.applyFilters()
        Log.i("DEBUG", "readFiltersFromStream end")
    }

    private fun readFiltersFromParser(parser: XmlPullParser) : Filters {
        var crit = ""
        var type = ""
        var option = ""
        val options = mutableListOf<String>()

        val filters = Filters()
        var eventType = parser.eventType

        while (!(eventType == XmlPullParser.END_TAG && parser.name == "filters")) {
            if (eventType == XmlPullParser.START_TAG) {
                if (parser.name == "filter") {
                    crit = parser.getAttributeValue(null, "crit")
                    type = parser.getAttributeValue(null, "type")
                } else if (parser.name == "option") {
                    val optionName = parser.getAttributeValue(null, "name")
                    if (type == "single") {
                        option = optionName
                    } else if (type == "mult") {
                        options.add(optionName)
                    }
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.name == "filter") {
                    when (crit) {
                        SortCrit.Completed.name -> {
                            filters.completionFilter =
                                if (type == "single") SingleFilter1(CompletionStatus.valueOf(option)) else FilterAll
                        }
                        SortCrit.DueDate.name -> {
                            filters.dueDateFilter =
                                if (type == "single") SingleFilter1(DateFilter.valueOf(option)) else FilterAll
                        }
                        SortCrit.Priority.name -> {
                            filters.prioFilter =
                                if (type == "mult") MultipleFilterM(options.map { Prio.valueOf(it) }) else FilterAll
                        }
                        SortCrit.Category.name -> {
                            filters.catFilter =
                                if (type == "mult") MultipleFilterM(options.map { Category(0, it) }) else FilterAll
                        }
                    }
                    options.clear()
                }
            }
            eventType = parser.next()
        }
        Log.i("DEBUG", "filters read from xml")
        return filters
    }

    fun readSortCritFromStream(inputStream: InputStream) {
        Log.i("DEBUG", "readSortCritFromStream start")
        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)

        val sortCrit = readSortCritFromParser(parser)
        tasks.setSortOrder(sortCrit)
        Log.i("DEBUG", "readSortCritFromStream end")
    }

    private fun readSortCritFromParser(parser: XmlPullParser): List<SortCrit> {
        val sortCrit = mutableListOf<SortCrit>()
        var orderBefore = 0
        var eventType = parser.eventType
        while (!(eventType == XmlPullParser.END_TAG && parser.name == "sorts")) {
            if (eventType == XmlPullParser.START_TAG) {
                if(parser.name == "sort") {
                    val order = parser.getAttributeValue(null, "order").toInt()
                    if (order == orderBefore + 1) {
                        val crit = SortCrit.valueOf(parser.getAttributeValue(null, "crit"))
                        sortCrit.add(crit)
                        orderBefore = order
                    } else {
                        throw Exception("sort order not ascending, $order following $orderBefore")
                    }
                }
            }
            eventType = parser.next()
        }
        Log.i("DEBUG", "sort criteria read from xml")
        return sortCrit
    }

    fun createExampleDataForStart() {

        // some categories
        with (categories) {
            addCategoryWithName("Garden")
            addCategoryWithName("House")
            addCategoryWithName("Finance")
            addCategoryWithName("Work")
        }

        // some tasks
        val taskDescription = "this is an example task, created automatically by toodoo. \n\n" +
                "you can change it, delete it, use it to learn about handling of the toodoo app."

        val task1 = Task()
        task1.name = "water flowers"
        task1.category = categories.categoryForName("Garden")
        task1.desc = taskDescription
        task1.priority = Prio.MEDIUM
        task1.dueDate = TDate.today
        task1.dueTime = TTime.nextFullHour
        tasks.addTask(task1)

        val task2 = Task()
        task2.name = "pick salad"
        task2.category = categories.categoryForName("Garden")
        task2.dueDate = TDate.tomorrow
        task2.priority = Prio.HIGH
        task2.desc = taskDescription
        tasks.addTask(task2)

        val task3 = Task()
        task3.name = "write letter to insurance"
        task3.category = categories.categoryForName("Finance")
        task3.priority = Prio.LOW
        task3.desc = taskDescription
        tasks.addTask(task3)
    }

    fun readAllDataFromStream(inputStream: InputStream) {
        Log.i("DEBUG", "readAllDataFromStream start")

        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)

        val categoriesFromXML = readCategoriesFromParser(parser)
        if (categoriesFromXML.isEmpty()) {
            throw Exception("no categories found in input, nothing imported")
        }

        val tasksFromXml = readTasksFromParser(parser)
        // for each task, replace dummy category with matching category
        tasksFromXml.forEach { task ->
            task.category = categoriesFromXML.firstOrNull { it.id == task.category.id } ?: throw Exception("task ${task.name} no category with id ${task.category.id} found")
        }

        val filtersFromXML = readFiltersFromParser(parser)
        // for catFilter, it is necessary to replace each dummy category with matching category
        filtersFromXML.catFilter.let { catFilter ->
            if(catFilter is MultipleFilterM<Category>) {
                val validCategories = catFilter.options.map { category ->
                    categoriesFromXML.firstOrNull { it.name == category.name } ?: throw Exception("for category filter, category with name ${category.name} not found")
                    }
                filtersFromXML.catFilter = MultipleFilterM(validCategories)
            }
        }

        val sortCritFromXML = readSortCritFromParser(parser)

        this.categories.setCategoriesFromStorage(categoriesFromXML)
        this.tasks.setTasksFromStorage(tasksFromXml)
        this.tasks.filters = filtersFromXML
        this.tasks.applyFilters()
        this.tasks.setSortOrder(sortCritFromXML)
        Log.i("DEBUG", "readAllDataFromStream end")
    }

    fun readTMLiteDataFromStream(inputStream: InputStream) {
        Log.i("DEBUG", "readTMLiteDataFromStream start")
        val factory = XmlPullParserFactory.newInstance()
        val parser = factory.newPullParser()
        parser.setInput(inputStream, null)

        val categoryList = mutableListOf<Category>()
        val taskList = mutableListOf<Task>()
        var taskTag = false
        var descTag = false
        var task = Task()
        this.tasks.resetNextId()
        this.categories.resetNextId()

        var eventType = parser.eventType
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if (parser.name == "task") {
                    task = Task()
                    task.id = this.tasks.getNextId()

                    val prioTML = parser.getAttributeValue(null, "priority").toInt()
                    task.priority = when (prioTML) {
                        4 -> Prio.LOW
                        3 -> Prio.MEDIUM
                        2 -> Prio.HIGH
                        else -> throw Exception("invalid priority ${prioTML} in file from TMLcategory")
                    }

                    val statusTML = parser.getAttributeValue(null, "status")
                    task.status = if (statusTML == "Completed") CompletionStatus.DONE else CompletionStatus.OPEN

                    // val due_date = Instant.ofEpochMilli(parser.getAttributeValue(null, "due_date").toLong())  // requires API level 26 or greater
                    val dueDateTML = parser.getAttributeValue(null, "due_date")
                    if(dueDateTML != null) {
                        val calenderD = Calendar.getInstance()
                        calenderD.time = Date(dueDateTML.toLong())

                        val year = calenderD.get(Calendar.YEAR).toShort()
                        val month = (calenderD.get(Calendar.MONTH) + 1).toByte()
                        val day = calenderD.get(Calendar.DAY_OF_MONTH).toByte()
                        task.dueDate = TDate(year,month,day)

                        val hour = calenderD.get(Calendar.HOUR_OF_DAY)
                        val minute = calenderD.get(Calendar.MINUTE)
                        // in TMLite, times 23:00 and 00:00 are often used to signal that no time is wanted at all (which TMLite cannot express explicitly)
                        task.dueTime = if((hour == 23 || hour == 0) && minute == 0) null else TTime(hour.toByte(), minute.toByte())
                    }

                    val creationDateTML = parser.getAttributeValue(null, "creation_date")
                    task.createDate.time = Date(creationDateTML.toLong())

                    val modifiedDateTML = parser.getAttributeValue(null, "modified_date")
                    task.lastModifiedDate.time = Date(modifiedDateTML.toLong())

                    val categoryTML = parser.getAttributeValue(null, "category")

                    val categoryForTask = categoryList.firstOrNull { it.name == categoryTML }
                    if (categoryForTask != null) {
                        task.category = categoryForTask
                    } else {
                        val newCategory = this.categories.createNewCategory(categoryTML)
                        categoryList.add(newCategory)
                        task.category = newCategory
                        Log.i("DEBUG", "new category created: ${newCategory.toStorageRep()}")
                    }
                    taskTag = true

                } else if (parser.name == "description") {
                    descTag = true
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.name == "task") {
                    taskTag = false
                    taskList.add(task)
                    Log.i("DEBUG", "task read from stream: ${task.toStorageRep()}")
                    task.desc.let { if(it.isNotEmpty()) { Log.i("DEBUG", "task desc: ${it}")} }
                } else if (parser.name == "description") {
                    descTag = false
                }
            } else if (eventType == XmlPullParser.TEXT) {
                val text = parser.text
                if (taskTag) {
                    if (descTag) {
                        task.desc = text
                    } else {
                        task.name = text
                    }
                }
            }
            eventType = parser.next()
        }
        Log.i("DEBUG", "${taskList.size} tasks read from TML xml")
        Log.i("DEBUG", "${categoryList.size}  categories read from TML  xml")
        if (categoryList.isNotEmpty() || taskList.isNotEmpty() ) {
            this.categories.setCategoriesFromStorage(categoryList)
            this.tasks.setTasksFromStorage(taskList)
            this.tasks.filters.resetToDefault()
            this.tasks.applyFilters()
            this.tasks.resetSortOrderToDefault()
        } else {
            throw Exception("no categories or tasks found in TML file, nothing imported")
        }
        Log.i("DEBUG", "readTMLiteDataFromStream end")
    }

}
