package com.wattwurm.toodoo.data

import java.util.*

data class TDate (val year: Short, val month: Byte, val day: Byte) : Comparable<TDate>{
    init {
        // checks for consistent month / day currently not needed, because date always entered via DatePicker and thus always valid
        if (month < 1 || month > 12) throw Exception("invalid month")
        if (day < 1 || day > 31) throw Exception("invalid day")
        if (day == 31.toByte() && listOf(2,4,6,9,11).contains(month.toInt())) throw Exception("invalid day/month")
    }

    companion object {
        fun fromStorageRep(rep: String): TDate? {
            return  if (rep.isEmpty()) null
            else TDate(
                rep.substring(0, 4).toShort(),
                rep.substring(5, 7).toByte(),
                rep.substring(8, 10).toByte()
            )
        }
        val today: TDate
            get() {
                val todayCalender = Calendar.getInstance() // today
                return fromCalendar(todayCalender)
            }
        val tomorrow: TDate
            get() {
                val tomorrowCalender = Calendar.getInstance() // today
                tomorrowCalender.add(Calendar.DATE, 1) // tomorrow
                return fromCalendar(tomorrowCalender)
            }
        private fun fromCalendar(cal: Calendar): TDate {
            val year = cal.get(Calendar.YEAR).toShort()
            val month = (cal.get(Calendar.MONTH) + 1).toByte()     // month in Calender is from 0 to 11
            val day = cal.get(Calendar.DAY_OF_MONTH).toByte()
            return TDate(year, month, day)
        }
    }

    fun isLessThan(other: TDate): Boolean {
        return (this.year < other.year)
                || (this.year == other.year && this.month < other.month)
                || (this.year == other.year && this.month == other.month && this.day < other.day)
    }

    fun addDays(numDays: Int): TDate {
        val newCal = this.toCalendar()
        newCal.add(Calendar.DATE, numDays)
        return fromCalendar(newCal)
    }

    val dayOfWeek: Weekday get() {
        val weekdayInt = this.toCalendar().get(Calendar.DAY_OF_WEEK)
        return if(weekdayInt == 1) Weekday.SUNDAY else Weekday.values()[weekdayInt-2]
    }

    val isLeapDay: Boolean get() {
        return month == 2.toByte() && day == 29.toByte()
    }

    private fun mondayBefore() : TDate {
        val cal = this.toCalendar()
        val dayOfWeek = cal.get(Calendar.DAY_OF_WEEK)
        if (dayOfWeek == 1) {
            cal.add(Calendar.DATE, -6)  // sunday, go back to monday before
        } else {
            cal.add(Calendar.DATE, 2-dayOfWeek)
        }
        return fromCalendar(cal)
    }

    fun isInCurrentWeek(): Boolean {
        return this.mondayBefore() == today.mondayBefore()
    }

    fun isMoreThanOneYearFromToday(): Boolean {
        val ttoday = today

        val highBound = TDate(
            year = (ttoday.year + 1).toShort(),
            month = ttoday.month,
            day = ttoday.day
        ).addDays(if(ttoday.isLeapDay) 0 else -1)
        if (highBound.isLessThan(this)) return true

        val lowBound = TDate(
            year = (ttoday.year - 1).toShort(),
            month = ttoday.month,
            day = ttoday.day
        ).addDays(if(ttoday.isLeapDay) 0 else 1)
        if (this.isLessThan(lowBound)) return true

        return false
    }

    fun isInNextWeek(): Boolean {
        return this.mondayBefore() == today.addDays(7).mondayBefore()
    }

    private fun toCalendar(): Calendar {
        val calender = Calendar.getInstance() // today
        calender.set(this.year.toInt(), this.month.toInt() - 1, this.day.toInt())
        return calender
    }

    override fun compareTo(other: TDate): Int {
        (this.year.compareTo(other.year)).let { if (it != 0) return it }
        (this.month.compareTo(other.month)).let { if (it != 0) return it }
        (this.day.compareTo(other.day)).let { if (it != 0) return it }
        return 0
    }

    fun toStorageRep(): String {
        val yearString = "0000${year}".takeLast(4)
        val monthString = "00${month}".takeLast(2)
        val dayString = "00${day}".takeLast(2)
        return "$yearString-$monthString-${dayString}"
    }

}


enum class Weekday {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    val shortName get() = when (this) {
        MONDAY -> "Mon"
        TUESDAY -> "Tue"
        WEDNESDAY -> "Wed"
        THURSDAY -> "Thu"
        FRIDAY -> "Fri"
        SATURDAY -> "Sat"
        SUNDAY -> "Sun"
    }
}

enum class Month {
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER;


    val shortName get() = when (this) {
        JANUARY -> "Jan"
        FEBRUARY -> "Feb"
        MARCH -> "Mar"
        APRIL -> "Apr"
        MAY -> "May"
        JUNE -> "Jun"
        JULY -> "Jul"
        AUGUST -> "Aug"
        SEPTEMBER -> "Sep"
        OCTOBER -> "Oct"
        NOVEMBER -> "Nov"
        DECEMBER -> "Dec"
    }
}
