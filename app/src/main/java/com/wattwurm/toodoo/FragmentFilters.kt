package com.wattwurm.toodoo

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.wattwurm.toodoo.data.CompletionStatus
import com.wattwurm.toodoo.data.DateFilter
import com.wattwurm.toodoo.data.Filters
import com.wattwurm.toodoo.data.Prio
import com.wattwurm.toodoo.databinding.DialogFilterBinding
import com.wattwurm.toodoo.databinding.FragmentFiltersBinding

class FragmentFilters : Fragment() {

    private lateinit var binding: FragmentFiltersBinding
    private lateinit var act: MainActivity
    private lateinit var filtersNew: Filters

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.i("DEBUG", "FragmentFilters.onCreateView")
        // Inflate the layout for this fragment
        binding = FragmentFiltersBinding.inflate(inflater)
        act = activity as MainActivity
        filtersNew = act.appState.tasks.filters.copy()

        binding.buttonCompletion.text = filtersNew.completionFilter.display
        binding.buttonCompletion.setOnClickListener {
            createDialogCompletionStatus()
        }

        binding.buttonDateFilter.text = filtersNew.dueDateFilter.display
        binding.buttonDateFilter.setOnClickListener {
            createDialogDateFilter()
        }

        binding.buttonPrioFilter.text = filtersNew.prioFilter.display
        binding.buttonPrioFilter.setOnClickListener {
            createDialogPrioFilter()
        }

        binding.buttonCatFilter.text = filtersNew.catFilter.display
        binding.buttonCatFilter.setOnClickListener {
            createDialogCatFilter()
        }

        binding.buttonCancel.setOnClickListener {
            act.supportFragmentManager.popBackStack()
        }

        binding.buttonDefault.setOnClickListener {
            filtersNew.resetToDefault()
            binding.buttonCompletion.text = filtersNew.completionFilter.display
            binding.buttonDateFilter.text = filtersNew.dueDateFilter.display
            binding.buttonPrioFilter.text = filtersNew.prioFilter.display
            binding.buttonCatFilter.text = filtersNew.catFilter.display
        }

        binding.buttonBackFromFilters.setOnClickListener {
            if (filtersNew != act.appState.tasks.filters) {
                act.appState.tasks.filters = filtersNew
                act.appState.tasks.applyFilters()
                act.writeFilters()
                Log.i("DEBUG", "FragmentFilters new filters were applied and saved")
            } else {
                Log.i("DEBUG", "FragmentFilters filters were not changed")
            }
            act.supportFragmentManager.popBackStack()
        }

        return binding.root
    }

    private fun createDialogDateFilter() {
        val dialogFilterBinding = DialogFilterBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this.context)
        builder
            .setMessage("Filter for due date:")
            .setView(dialogFilterBinding.root)
            .setPositiveButton("OK") { dialog, id -> }
            .setNegativeButton("Cancel") { dialog, id -> }
        val dialog = builder.create()
        dialog.show()

        // accessing the recycler view must be done after (!) dialog.show()
        val recyclerView = dialogFilterBinding.filterDialogRecycler
        val adapter = SingleFilterAdapter(
            DateFilter.values().toList(),
            filtersNew.dueDateFilter
        )
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { view ->
            // close the dialog,
            dialog.dismiss()
            // use the values entered in the dialog to set new filter and text for filter buttons
            val newFilter = adapter.currentFilter
            filtersNew.dueDateFilter = newFilter
            binding.buttonDateFilter.text = newFilter.display
        }
    }

    private fun createDialogPrioFilter() {
        val dialogFilterBinding = DialogFilterBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this.context)
        builder
            .setMessage("Filter for priority:")
            .setView(dialogFilterBinding.root)
            .setPositiveButton("OK") { dialog, id -> }
            .setNegativeButton("Cancel") { dialog, id -> }
        val dialog = builder.create()
        dialog.show()

        // accessing the recycler view must be done after (!) dialog.show()
        val recyclerView = dialogFilterBinding.filterDialogRecycler
        val adapter = MultipleFilterAdapter(
            Prio.values().toList(),
            filtersNew.prioFilter
        )
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { view ->
            // close the dialog,
            dialog.dismiss()
            // use the values entered in the dialog to set new filter and text for filter buttons
            val newFilter = adapter.currentFilter
            filtersNew.prioFilter = newFilter
            binding.buttonPrioFilter.text = newFilter.display
        }
    }

    private fun createDialogCompletionStatus() {
        val dialogFilterBinding = DialogFilterBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this.context)
        builder
            .setMessage("Filter for completion status:")
            .setView(dialogFilterBinding.root)
            .setPositiveButton("OK") { dialog, id -> }
            .setNegativeButton("Cancel") { dialog, id -> }
        val dialog = builder.create()
        dialog.show()

        // accessing the recycler view must be done after (!) dialog.show()
        val recyclerView = dialogFilterBinding.filterDialogRecycler
        val adapter = SingleFilterAdapter(
            CompletionStatus.values().toList(),
            filtersNew.completionFilter
        )
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { view ->
            // close the dialog,
            dialog.dismiss()

            // use the values entered in the dialog to set new filter and text for filter buttons
            val newFilter = adapter.currentFilter
            filtersNew.completionFilter = newFilter
            binding.buttonCompletion.text = newFilter.display
        }
    }

    private fun createDialogCatFilter() {
        val dialogFilterBinding = DialogFilterBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this.context)
        builder
            .setMessage("Filter for category:")
            .setView(dialogFilterBinding.root)
            .setPositiveButton("OK") { dialog, id -> }
            .setNegativeButton("Cancel") { dialog, id -> }
        val dialog = builder.create()
        dialog.show()

        // accessing the recycler view must be done after (!) dialog.show()
        val recyclerView = dialogFilterBinding.filterDialogRecycler
        val adapter = MultipleFilterAdapter(
            act.appState.categories.iterator.asSequence().toList(),
            filtersNew.catFilter
        )
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { view ->
            // close the dialog,
            dialog.dismiss()
            // use the values entered in the dialog to set new filter and text for filter buttons
            val newFilter = adapter.currentFilter
            filtersNew.catFilter = newFilter
            binding.buttonCatFilter.text = newFilter.display
        }
    }

}