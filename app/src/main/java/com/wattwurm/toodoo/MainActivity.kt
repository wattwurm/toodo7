package com.wattwurm.toodoo

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.wattwurm.toodoo.data.AppState
import com.wattwurm.toodoo.databinding.ActivityMainBinding
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    private val request_external_storage = 17  // any number can be used, simply must be the same in requestPermissions and onRequestPermissionsResult

    private val fileNameTasks = "tasks.xml"
    private val fileNameCategories = "categories.xml"
    private val fileNameFilters = "filters.xml"
    private val fileNameSortCrit = "sorts.xml"

    // todo   is there a better way to access data from activities and fragments, e.g. ViewModel
    val appState = AppState()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        readCategories()
        readTasks()
        readFilters()
        readSortCrit()

        if (appState.categories.countItems == 0 && appState.tasks.countAllTasks == 0) {
            appState.createExampleDataForStart()
            writeCategories()
            writeTasks()
            Toast.makeText(
                this.applicationContext,
                "toodoo app initial start",
                Toast.LENGTH_LONG).show()
            Toast.makeText(
                this.applicationContext,
                "${appState.categories.countItems} example categories and ${appState.tasks.countAllTasks} example tasks were created",
                Toast.LENGTH_LONG).show()
        }

        val binding = ActivityMainBinding.inflate(layoutInflater)
        //setContentView(R.layout.activity_main)
        setContentView(binding.root)

        showTaskList()
    }

    fun writeCategories() {
        try {
            Log.i("DEBUG", "writeCategories start")
            val fos = openFileOutput(fileNameCategories, Context.MODE_PRIVATE)
            appState.writeCategoriesToStream(fos)
        } catch (e: Exception) {
            val message = "error saving categories - Exception $e "
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        }
    }

    private fun readCategories() {
        Log.i("DEBUG", "readCategories start")
        try {
            val fis = this.openFileInput(fileNameCategories)
            appState.readCategoriesFromStream(fis)
            // Toast.makeText(applicationContext, "${appState.categories.countItems} categories read from xml", Toast.LENGTH_SHORT).show()
        } catch (e: FileNotFoundException) {
            Log.i("DEBUG", "error reading categories, file $fileNameCategories not found, $e")
        } catch (e: Exception) {
            val message = "error reading categories: $e"
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        }
    }

    private fun readTasks() {
        Log.i("DEBUG", "readTasks start")
        try {
            val fis = this.openFileInput(fileNameTasks)
            appState.readTasksFromStream(fis)
        } catch (e: FileNotFoundException) {
            Log.i("DEBUG", "error reading tasks, file $fileNameCategories not found, $e")
        } catch (e: Exception) {
            val message = "error reading tasks: $e"
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        }
    }

    private fun readFilters() {
        Log.i("DEBUG", "readFilters start")
        try {
            val fis = this.openFileInput(fileNameFilters)
            appState.readFiltersFromStream(fis)
        } catch (e: FileNotFoundException) {
            Log.i("DEBUG", "error reading filters, file $fileNameFilters not found, $e")
        } catch (e: Exception) {
            val message = "error reading filters: $e"
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        }
    }

    private fun readSortCrit() {
        Log.i("DEBUG", "readSortCrit start")
        try {
            val fis = this.openFileInput(fileNameSortCrit)
            appState.readSortCritFromStream(fis)
        } catch (e: FileNotFoundException) {
            Log.i("DEBUG", "error reading sort criteria, file $fileNameSortCrit not found, $e")
        } catch (e: Exception) {
            val message = "error reading sort criteria: $e"
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        }
    }

    fun writeTasks() {
        try {
            Log.i("DEBUG", "writeTasks start")
            val fos = openFileOutput(fileNameTasks, Context.MODE_PRIVATE)
            appState.writeTasksToStream(fos)
        } catch (e: Exception) {
            val message = "error saving tasks - Exception $e "
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        }
    }

    fun writeFilters() {
        try {
            Log.i("DEBUG", "writeFilters start")
            val fos = openFileOutput(fileNameFilters, Context.MODE_PRIVATE)
            appState.writeFiltersToStream(fos)
        } catch (e: Exception) {
            val message = "error saving filters - Exception $e "
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        }
    }

    fun writeSortCrit() {
        try {
            Log.i("DEBUG", "writeSortCrit start")
            val fos = openFileOutput(fileNameSortCrit, Context.MODE_PRIVATE)
            appState.writeSortCritToStream(fos)
        } catch (e: Exception) {
            val message = "error saving sort criteria - Exception $e "
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        }
    }

    fun showTaskList() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentTaskLst()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        // fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun showTaskListSecondLevel() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentTaskLst()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun showTaskDetail() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentDetail()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun showCatList() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentCategories()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun showFilters() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentFilters()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun showSortCrit() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragment = FragmentSortCriteria()
        fragmentTransaction.replace(R.id.mainContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun checkAndWriteTasksToExt() {
        // val permission = ActivityCompat.checkSelfPermission(
        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (permission == PackageManager.PERMISSION_GRANTED) {
            writeDataToExtFile()
        } else {
            // We don't have permission so prompt the user
            Log.i("DEBUG", "requesting permission from user ${Manifest.permission.WRITE_EXTERNAL_STORAGE}")
            Log.i("DEBUG", " request code 17")

            val stringArray = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            // requestPermissions(stringArray, request_external_storage);
            ActivityCompat.requestPermissions(
                this,
                stringArray,
                request_external_storage
            )
        }
    }

    private fun writeDataToExtFile() {
        try {
            Log.i("DEBUG", "writeDataToExtFile start")
            val downloadFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val subdir = File(downloadFolder,"toodoo")
            if (!subdir.isDirectory) {  // also checks if existing
                subdir.mkdir()
            }
            val currentDateTime: Calendar = Calendar.getInstance()
            val sdf = SimpleDateFormat("yyyy-MM-dd-HH:mm:ss:SSS" )
            val currentDateS = sdf.format(currentDateTime.time)
            val filename = "toodooData-${currentDateS}.xml"
            val file = File(subdir,filename)
            Log.i("DEBUG", "creating file ${file}")
            file.createNewFile()
            if(file.exists()) {
                val output = FileOutputStream(file)
                appState.writeAllDataToStream(output)
                Log.i("DEBUG", "data exported to $file")
                Toast.makeText(applicationContext, "file saved in folder ${downloadFolder}", Toast.LENGTH_LONG).show()
                Toast.makeText(applicationContext, "file name is $filename", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(applicationContext, "output to external file not possible, file could not be created ", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            val message = "error exporting data to external file - Exception $e "
            Log.i("DEBUG", message)
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        // this is called when the requested permission is returned to this activity
        Log.i("DEBUG", "onRequestPermissionsResult start")
        Log.i("DEBUG", "requestCode is $requestCode")
        Log.i("DEBUG", "permissions size is ${permissions.size}")
        Log.i("DEBUG", "permissions first elem  is ${permissions[0]}")
        Log.i("DEBUG", "grantResults size is ${grantResults.size}")
        Log.i("DEBUG", "grantResults first elem  is ${grantResults[0]}")

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == request_external_storage) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                writeDataToExtFile()
            }
            else {
                Toast.makeText(applicationContext,
                    "Storage Permission Denied",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    fun createDialogAbout() {
        val builder = AlertDialog.Builder(this)
        builder
            //.setMessage("toodoo")
            .setView(layoutInflater.inflate(R.layout.dialog_about, null))  // results in warning null as root should be avoided - but in dialog not possible otherwise
            .setPositiveButton("OK") { dialog, id -> }
        //.setNegativeButton("Cancel") { dialog, id -> } // User cancelled the dialog
        val dialog = builder.create()
        dialog.show()
    }

}
